angular.module('crudApp')

.constant('baseUrl', 'http://localhost:8080/api')

.service('customersService', function($q, $http, baseUrl) {
	var customersService = {};
	var deffered = $q.defer();
	var data = [];

	customersService.getAllCustomers = function() {
		$http({
			method: 'GET',
			url: baseUrl + '/customers',
			headers: {"Content-Type": "application/json"}
		})
		.success(function(d) {
			data = d;
			deffered.resolve();
		})
		.error(function(err) {
			console.log(err);
			deffered.reject();
		});
		return deffered.promise;
	};

	customersService.data = function() {
		return data;
	}


	customersService.createCustomer = function(customerData) {
		customerData.phone_number = '+381' + customerData.phone_number;
		$http({
			method: "POST",
			url: baseUrl + '/customers',
			data: customerData,
			headers: {"Content-Type": "application/json"}
		})
		.success(function() {
			deffered.resolve();
		})
		.error(function(err) {
			console.log(err);
			deffered.reject();
		});

		return deffered.promise;
	}	


	customersService.deleteCustomer= function(id) {
		$http({
			method: 'DELETE',
			url: baseUrl + '/customers/' + id,
			headers: {"Content-Type": "application/json"}
		})
		.success(function() {
			deffered.resolve();
		})
		.error(function(err) {
			console.log(err);
			deffered.reject();
		});

		return deffered.promise;
	}

	customersService.editCustomer= function(customer) {
		$http({
			method: 'PUT',
			url: baseUrl + '/customers/' + customer.id,
			data: customer,
			headers: {"Content-Type": "application/json"}
		})
		.success(function() {
			deffered.resolve();
		})
		.error(function(err) {
			console.log(err);
			deffered.reject();
		});

		return deffered.promise;
	}


	return customersService;
})

.service('authService', function($http, $q, $rootScope, baseUrl) {
	var deffered = $q.defer();
	var authService = {};

	authService.login = function(user) {
		$http({
			method: 'POST',
			url: baseUrl + '/login',
			data: user,
			headers: {"Content-Type": "application/json"}
		})
		.success(function(response) {
			$rootScope.isLogged = true;
			deffered.resolve();
		})
		.error(function(err) {
			deffered.reject();
		});

		return deffered.promise;
	}

	authService.logout = function() {
		$rootScope.isLogged = false;
	};

	return authService;
}); 