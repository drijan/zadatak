angular.module('crudApp', ['ngRoute', 'ui.bootstrap', 'toastr'])

.config(function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: 'views/login.html',
			controller: 'LoginCtrl'
		})
		.when('/customers', {
			templateUrl: 'views/customers.html',
			controller: 'CustomersCtrl'	
		})
		.otherwise({
	        redirectTo: '/'
	    });
})

.run(function($rootScope, $location, toastr) {
	$rootScope.$on('$locationChangeStart', function(event, next, current) {
      // redirect to home page if user isn't logged in
      if($location.path() !== '/') {
        if($location.path() !== '/' && $rootScope.isLogged === false) {
        	toastr.warning("Please log in!");
        	$location.path('/');
        }
      }
    });
});
