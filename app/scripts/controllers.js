angular.module('crudApp')
.controller('LoginCtrl', function($scope, $rootScope, $location, authService, toastr) {
	$scope.user = null;

	$rootScope.isLogged = false;

	$scope.login = function(user) {
		
		authService.login(user) 
			.then(function() {
				toastr.success("Successfuly logged in!");
				$location.path('/customers');
			})
			.catch(function() {
				toastr.error("Check your username and password and try again!")
			});
	};
})

.controller('CustomersCtrl', function($scope, $rootScope, $uibModal, customersService, $location, $interval, toastr, authService) {

	customersService.getAllCustomers()
		.then(function() {
			$scope.allCustomers = customersService.data();
			
			// very dummy way to remove +381
			$scope.allCustomers.forEach(function(customer) {
				if(customer.phone_number.indexOf('+381') !== -1) {
					customer.phone_number = customer.phone_number.slice(3, customer.phone_number.length);
				}
			});
		})
		.catch(function(err) {
			// TODO: handle error
		});

	$scope.sortType = "";
	$scope.sortReverse = false;
    $scope.sortReverseName = false;
    $scope.sortReverseNumber = false;

    // 
    $scope.openCreateModal = function() {
    	var modalInstance = $uibModal.open({
	      	animation: $scope.animationsEnabled,
	     	templateUrl: 'views/templates/create-customer.tpl.html',
	      	controller: 'ModalInstanceCtrl',
	      	resolve: {
	      		customer: function() {
	      			return;
	      		}
	      	}
    	});    	
    };

    $scope.openEditModal = function(customer) {
    	var modalInstance = $uibModal.open({
	      	animation: $scope.animationsEnabled,
	     	templateUrl: 'views/templates/edit-customer.tpl.html',
	      	controller: 'ModalInstanceCtrl',
	      	resolve: {
	      		customer: function() {
	      			return customer;
	      		}
	      	}
    	});    	
    };

    $scope.openDeleteModal = function(customer) {
    	var modalInstance = $uibModal.open({
	      	animation: $scope.animationsEnabled,
	     	templateUrl: 'views/templates/delete-customer.tpl.html',
	      	controller: 'ModalInstanceCtrl',
	      	resolve: {
	      		customer: function() {
	      			return customer;
	      		}
	      	}
    	});    	
    };


    $scope.logout = function() {
    	// TODO real logout
    	$location.path('/');
    	authService.logout();
    };	

    // rootScope is only parent of ModalInstanceCtrl, so I use it to remove deleted customer
    $rootScope.$on('DELETE', function(event, data) {
    	$scope.allCustomers = $scope.allCustomers.filter(function(el) {
    		return el.id !== data;
    	});    	
    });

    $rootScope.$on('EDIT', function(event, data) {
    	console.log(data);
    	$scope.allCustomers.forEach(function(el) {
    		if(el.id === data.id) {
    			el.customer_name = data.customer_name;
    			el.phone_number = data.phone_number;
    			el.priority = data.priority;
    		}
    	});
    });

    $rootScope.$on('CREATE', function(event, data) {
			if(data.phone_number.indexOf('+381') !== -1) {
				data.phone_number = data.phone_number.slice(3, data.phone_number.length);
			}
    	$scope.allCustomers.push(data);
    });

})

.controller('ModalInstanceCtrl', function($scope, $uibModalInstance, customersService, customer, toastr) {
 
	$scope.customer = customer;

	// for storing changed customer's data for UPDATE 
	$scope.changedCustomer = {
		id: 0,
		customer_name: '',
		phone_number: '',
		priority: ''
	};

	$scope.priorities = ['premium', 'suspended'];

	$scope.createCustomer = function(customer) {
		customersService.createCustomer(customer)
			.then(function() {
				$uibModalInstance.dismiss();
				// emit create event with newly created customer info
				$scope.$emit('CREATE', customer);
				toastr.success("Successfuly created customer!");
			})
			.catch(function() {
				// TODO handle error
				toastr.error("Error");
			})
	};


	$scope.editCustomerInfo = function(customer) {
		// very dummy way to remove +381
		if(customer.phone_number.indexOf('+381') !== -1) {
			customer.phone_number = customer.phone_number.slice(3, customer.phone_number.length);
		}
		customersService.editCustomer(customer)
 			.then(function() {
				$uibModalInstance.dismiss();

				// emit edit event with updated customer info
				$scope.$emit('EDIT', customer);
				toastr.success("Customer info updated!")
			})
			.catch(function(err) {
				// TODO handle error 
				toastr.error("Error");
			});
	};

    $scope.deleteCustomer = function(customer) {
    	var id = customer.id;
    	customersService.deleteCustomer(id)
    		.then(function() {
    			$uibModalInstance.dismiss();
    			toastr.success("Customer deleted!")
    			// emit delete event to rootScope 
    			$scope.$emit('DELETE', customer.id);
    		})
    		.catch(function(err) {
    			// TODO: handle error
    			toastr.error("Error");
    		});
    };

	$scope.closeModal = function() {
		$uibModalInstance.dismiss();
	};
});