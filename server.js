var express = require('express'),
	path = require('path'),
	bodyParser = require('body-parser'),
	app = express(),
	expressValidator = require('express-validator')

app.use(express.static(__dirname + '/app'));
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json());
app.use(expressValidator());


var connection = require('express-myconnection'),
	mysql = require('mysql');

app.use(
	connection(mysql, {
		host: 'localhost',
		user: 'root',
		// password: '',
		database: 'callerid',
	}, 'request')
);



// RESTful route
var router = express.Router();

router.use(function(req, res, next) {
	console.log(req.method, req.url);
	next();
});


// get all customers
var curut = router.route('/customers');

curut.get(function(req, res, next) {
	req.getConnection(function(err, conn) {
		if(err) {
			return next("Cannot Connect");
		}
		var query = conn.query('SELECT * FROM customers', function(err, rows) {
			if(err) {
				console.log(err);
				return next("Mysql error, check your query");
			}
			var string = JSON.stringify(rows);
			var json = JSON.parse(string);
			res.send(json);
		});
		
	});
});


// create a new customer
curut.post(function(req, res, next) {

	// get data
	var data = {
		customer_name: req.body.customer_name,
		phone_number: req.body.phone_number,
		priority: req.body.priority
	};

	// insert into mysql
	req.getConnection(function(err, conn) {
		if(err) {
			return next("Cannot Connect");
		}

		var query = conn.query("INSERT INTO customers set ? ", data, function(err, rows) {
			if(err) {
				console.log(err);
				return next("Mysql error, check your query");
			}

			res.sendStatus(201);
		});
	});
});


var curut2 = router.route('/customers/:id');

// update customer
curut2.put(function(req, res, next) {
	var id = req.params.id;

	// get data
	var data = {
		customer_name: req.body.customer_name,
		phone_number: req.body.phone_number,
		priority: req.body.priority
	};

	// insert into mysql
	req.getConnection(function(err, conn) {
		if(err) {
			return next("Cannot Connect");
		}

		var query = conn.query("UPDATE customers set ? WHERE id = ? ", [data, id], function(err, rows) {
			if(err) {
				console.log(err);
				return next("Mysql error, check query");
			}
			res.sendStatus(200);
		});
	});
});

// delete customer
curut2.delete(function(req, res, next) {
	var id = req.params.id;

	req.getConnection(function(err, conn) {
		if(err) {
			return next("Cannot Connect");
		}

		var query = conn.query("DELETE FROM customers WHERE id = ? ", [id], function(err, rows) {
			if(err) {
				console.log(err);
				return next("Mysql error");
			}
			res.sendStatus(200);
		});
	});
});


// login 
var curut3 = router.route('/login');
curut3.post(function(req, res, next) {
	var user = {
		username: req.body.username,
		password: req.body.password
	};

	req.getConnection(function(err, conn) {
		console.log(req.body);
		if(err) {
			return next("Cannot Connect");
		}

		var query = conn.query("SELECT username, password FROM users WHERE username = ? ", [user.username], function(err, rows) {
			if(err) {
				console.log(err);
				return next("Mysql error, check query");
			}

			console.log(rows);
			
			if(rows.length > 0) {
				var string = JSON.stringify(rows);
				var json = JSON.parse(string); // --> array
				if(json[0].password == user.password) {
					res.sendStatus(200);
				} else {
					res.sendStatus(401);
				}
			} else {
				res.sendStatus(401);
			}

		});
	});
});

app.use('/api', router);




app.get('/', function(req, res) {
	res.sendFile(__dirname + '/index.html');
});

app.listen(8080, function() {
	console.log("listening on port 8080");
});	